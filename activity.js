

// Activity S24

// Solution 1
db.users.find(
	{
		$or: [
			{ firstName: {$regex: "S", $options: "i"} },
			{ lastName: {$regex: "D", $options: "i"} }
		]
	},
		{ _id: 0, firstName: 1, lastName: 1 }
);

// Solution 2
db.users.find(
	{
		$and: [
			{ department: "HR"}, {age: {$gte: 30} }
		]
	}
);

// Solution 3
db.users.find(
	{
		$and: [
			{ $or: [{firstName: {$regex: "e", $options: "i"}}, {lastName: {$regex: "e", $options: "i"}}] },
			{ age: {$lte: 30} }
		]
	}
);